module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),


    // Tasks
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: ['assets/js/plugins/**/*.js', 'assets/js/main.js'],
        dest: 'assets/js/build/production.js',
      },
    },

    

    cssmin: {
      files: {'style.min.css': 'style.css'},
    },    

    uglify: {
      build: {
        src: 'assets/js/build/production.js',
        dest: 'assets/js/build/production.min.js'
      }
    },

    watch: {
      scripts: {
        files: ['*'],
        tasks: ['concat', 'uglify','cssmin',],
        options: {
          spawn: false,
          livereload: true,
        },
      } 
    }

  });

  // Load Plugins
  grunt.loadNpmTasks('grunt-contrib-concat'); // npm install grunt-contrib-concat --save-dev
  // grunt.loadNpmTasks('grunt-contrib-less');   // npm install grunt-contrib-less --save-dev
  grunt.loadNpmTasks('grunt-contrib-cssmin'); // npm install grunt-contrib-cssmin --save-dev
  grunt.loadNpmTasks('grunt-contrib-uglify'); // npm install grunt-contrib-uglify --save-dev
  grunt.loadNpmTasks('grunt-contrib-watch');  // npm install grunt-contrib-watch --save-dev
  

  // Make it grunt
  grunt.registerTask('default', ['concat', 'cssmin', 'uglify', 'watch']);

};